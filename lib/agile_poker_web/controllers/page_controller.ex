defmodule AgilePokerWeb.PageController do
  use AgilePokerWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
